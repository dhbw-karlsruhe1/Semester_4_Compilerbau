# Repository zu den Hausaufgaben der Vorlesung Compilerbau an der DHBW Karlsruhe

Struktur:
- [programme](programme) : Ausarbeitungen der Hausaufgaben
- [programme_orginal](programme_orginal) : Unbearbeiteter Aufgaben Ordner
- [programme-2022-03-25.tgz](programme-2022-03-25.tgz) : tar gz des unbearbeiteten Aufgaben-Ordners
- [programme/Aufgabe-Verzeichnis.md](programme/Aufgabe-Verzeichnis.md) : Auflistung der Hausaufgaben, wo unsere Lösung ist, was die Aufgabenstellung war & wie viele Punkte erreicht wurden