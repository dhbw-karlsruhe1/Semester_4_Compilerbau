/* Project:  Vorlesung Uebersetzerbau
 * Descr:    Aufbau AST in JAVA
 * Kind:     Liste von Ausdruecken: Listenknoten
 * Author:   Prof. Dr. Juergen Vollmer <vollmer@dhbw-karlsruhe.de>
 * $Id: Exprs.java.in,v 1.3 2012/10/20 14:00:50 vollmer Exp $
 */

class Exprs extends EXPRS
{
    public EXPR  Expr;
    public EXPRS Next;
    public Exprs (EXPR _Expr, EXPRS _Next)
    {
	Expr = _Expr;
	Next = _Next;
    }

    public void write_and_eval()
    {
	Expr.write();
	System.out.print(" = ");
	Expr.write();
	System.out.print("\n");

	Next.write_and_eval();
    }
}
