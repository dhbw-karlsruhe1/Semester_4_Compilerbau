/* Project:  Vorlesung Uebersetzerbau
 * Descr:    Aufbau AST in JAVA
 * Kind:     Klasse fuer IntConst-Knoten
 * Author:   Prof. Dr. Juergen Vollmer <vollmer@dhbw-karlsruhe.de>
 * $Id: Name.java,v 1.1 2020/07/05 13:30:14 vollmer draft $
 */
class Name extends EXPR
{
    public  String  ident;
    private Storage storage;
    public Name (String _ident)
    {
	ident = _ident;
	Storage.declare (ident);
    }

    public void write()
    {
	System.out.print (ident);
    }

    public double eval()
    {
	return Storage.get (ident);
    }
}
