/* Project:  Vorlesung Uebersetzerbau
 * Descr:    Aufbau AST in JAVA
 * Kind:     Klasse fuer BinaryExpr-Knoten
 * Author:   Prof. Dr. Juergen Vollmer <vollmer@dhbw-karlsruhe.de>
 * $Id: BinaryExpr.java,v 1.2 2020/07/05 13:32:17 vollmer Exp $
 */
class BinaryExpr extends EXPR
{
    public char op;
    public EXPR left;
    public EXPR right;
    public BinaryExpr (char _op, EXPR _left, EXPR _right)
    {
	op    = _op;
	left  = _left;
	right = _right;
    }

    public void write()
    {
	System.out.print("(");
	left.write();
	System.out.print(op);
	right.write();
	System.out.print(")");
    }
    public double eval()
    {
	switch (op) {
	case '+': return left.eval() +
		  right.eval();
	case '-': return left.eval() -  right.eval();
	case '*': return left.eval() *  right.eval();
	case '/': return left.eval() /  right.eval();
	case '<': return left.eval() <  right.eval() ? 1.0 : 0.0;
	case '=': return left.eval() == right.eval() ? 1.0 : 0.0;
	case '>': return left.eval() >  right.eval() ? 1.0 : 0.0;
	default:  System.out.println("ERROR: BinaryExpr.eval(op=<"+op+"> undefined");
	          java.lang.System.exit(1);
		  return 0;
	}
    }
}
