/* Project:  Vorlesung Uebersetzerbau
 * Descr:    Aufbau AST in JAVA
 * Kind:     Klasse fuer IntConst-Knoten
 * Author:   Prof. Dr. Juergen Vollmer <vollmer@dhbw-karlsruhe.de>
 * $Id: Storage.java,v 1.1 2020/07/05 13:30:14 vollmer draft $
 */
import java.util.HashMap;
class Storage
{
    // Hashmap VariableName -> Wert der Variable
    private static HashMap<String, Double> storage;

    // Eine (versteckte) Klassenvariable vom Typ der eigenen Klasse
    private static Storage instance;

    // Verhindere die Erzeugung des Objektes über andere Methoden
    private Storage() {}

    // Eine Zugriffsmethode auf Klassenebene, welches '''einmal''' ein konkretes
    // Objekt erzeugt und dieses zurückliefert.
    public static Storage getInstance() {
	if (Storage.instance == null) {
	    Storage.instance = new Storage ();
	    Storage.storage  = new HashMap<String, Double>();
	}
	return Storage.instance;
    }

    public static void write ()
    {
	System.out.print("Storage = ");
	System.out.println(storage);
    }

    public static void declare(String Name)
    {
	storage.put (Name, 0.0);
    }

    public static void set(String Name, double Value)
    {
	storage.put (Name, Value);
    }

    public static double get(String Name)
    {
	if (!Storage.storage.containsKey(Name)) {
	    System.out.println("ERROR: <"+Name+"> has no value");
	    java.lang.System.exit(1);
	}
	return storage.get (Name);
    }
}
