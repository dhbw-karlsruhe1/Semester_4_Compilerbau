/* Project:  Vorlesung Uebersetzerbau
 * Descr:    Aufbau AST in JAVA
 * Kind:     Abstrakte Klasse EXPR
 * Author:   Prof. Dr. Juergen Vollmer <vollmer@dhbw-karlsruhe.de>
 * $Id: AssignStmt.java,v 1.2 2020/07/05 13:31:33 vollmer Exp $
 */
class AssignStmt extends STMT
{
    public Name name;
    public EXPR expr;
    public AssignStmt (Name _name, EXPR _expr)
    {
	super (new NoStmt());
	name = _name;
	expr = _expr;
    }
    public AssignStmt (STMTS _next, Name _name, EXPR _expr)
    {
	super (_next);
	name = _name;
	expr = _expr;
    }
    public void interprete()
    {
	Storage.set (name.ident, expr.eval());
	Next.interprete();
    }
    public void write()
    {
	name.write();
	System.out.print (" := ");
	expr.write();
	System.out.println (";");

	Next.write();
    }
}
