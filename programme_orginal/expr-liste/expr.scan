/* Project:  COCKTAIL training
 * Descr:    A simple pocket computer (scanner, parser, evaluator)
 * Kind:     Scanner specification
 * Author:   Prof. Dr. Juergen Vollmer <vollmer@dhbw-karlsruhe.de>
 * $Id: expr.scan,v 1.3 2010/04/23 20:39:36 vollmer Exp $
 */

SCANNER expr_scan

EXPORT {
/* code to be put intp Scanner.h */
# include "Position.h"

/* Token Attributes.
 * For each token with user defined attributes, we need a typedef for the
 * token attributes.
 * LPP extracts the token-attribute declaration from the parser specification.
 * They are inserted here.
 */
INSERT tScanAttribute
}

GLOBAL {
/* code to be put into Scanner.c */
# include <stdlib.h>
# include "Errors.h"

/* Insert the routine computing "error-values" of attributes, in case the
 * parser decides during error repair to insert a token.
 */
INSERT ErrorAttribute
}

LOCAL {
}

DEFAULT {
  /* What happens if no scanner rule matches the input */
  MessageI ("Illegal character",
	    xxError, expr_scan_Attribute.Position,
	    xxCharacter, expr_scan_TokenPtr);
}

EOF {
  /* What should be done if the end-of-input-file has been reached? */
  /* implicit: return the EofToken */
}

DEFINE /* some abbreviations */
  letter = {a-zA-Z_}   .
  digit  = {0-9}       .

/* define start states, note STD is defined by default */
// START COMMENT, COMMENT2

RULES

/* Integers */
#STD# {0-9}+ :
	{expr_scan_Attribute.int_const.Value = atol (expr_scan_TokenPtr);
	 return int_const;
	}

#STD# < "--" ANY * > :
        { /* comment up to end of line, nothing to do */
        }

INSERT RULES #STD#


/**********************************************************************/
