# Sprache X: Scanner (2 Punkte)
Implementieren Sie für die von Ihnen definierte Programmiersprache X einen Scanner mit dem Werkzeug REX.

Der Scanner soll alle Tokens Ihrer Sprache akzeptieren, welche Attribute haben (also Zahlen, Bezeichner, Strings). Vergessen Sie nicht die Tokenattribute  (vom Typ tIdent, tStringRef, int etc.) zu bestimmen. Des weiteren sollen zwei Schlüsselwörter und der Operator "+" vom Scanner erkannt werden. Die anderen Symbole Ihrer Sprache dürfen ignoriert werden.

Der Scanner soll Kommentare behandeln, die Sie hoffentlich in Ihrer Sprache definiert haben. Zu Testzwecken wird der Scanner in einer Schleife aufgerufen (Siehe Programmierübung scanner).

Erstellen Sie einige Test-Programme für Ihre Sprache. Es sollen sowohl korrekte als auch syntaktisch und semantisch falsche Tests geben.

Bitte erstellen Sie ein komprimiertes tar-File der Lösung (nur Programmquellen) und laden Sie dies hoch.

Ich gehe davon aus, dass Ihre Programmquellen im Verzeichnis ~/programme/Name1-Name2-Name3 liegen.

Bitte gehen Sie so vor (im SuSE Gast-System);

    cd programme

    tar -czvf Name1-Name2-Name3.tgz   Name1-Name2-Name3

Ich werde das tar-Archiv auspacken und in diesem Verzeichnis

    make clean

    make

    make test

ausführen.

Bitte legen Sie Ihre Sprachdefinition in dem Verzeichnis ab, so dass ich sie mir anschauen kann.

---

## Feedback
> Bezeichner sollten als Typ tIdent haben. Strings dann tStringRef warum hat operator ein Attribut? In derParseraufgabe dann fixen....

## Punkte:

2,00/2,00