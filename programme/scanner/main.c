/* Project:  COCKTAIL training
 * Descr:    A simple scanner generated with rex
 * Kind:     C-main function, fully implemented solution
 * Author:   Dr. Juergen Vollmer <juergen.vollmer@informatik-vollmer.d
 * $Id: main.c.in,v 1.6 2018/04/25 14:58:12 vollmer Exp $
 */

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include "Position.h"
# include "l_scan.h"

int main (int argc, char *argv[])
{
   int Token, Count = 0;
   if (argc == 2) {
     if (strcmp (argv[1], "-h") == 0) {
       fprintf (stderr,
		"usage: scan [-h] [file]\n"
		"  simple scanner, reads `file' or stdin\n"
		"  -h: Help\n");
       exit (0);
     }
     l_scan_Attribute.Position.FileName = MakeIdent1 (argv[1]);
     l_scan_BeginFile (argv[1]);
     /* Read from file argv[1].
      * If this routine is not called, stdin is read.
      */
   }
   for (Token =  l_scan_GetToken ();
	Token != l_scan_EofToken;
	Token = l_scan_GetToken ()) {
      Count ++;
      WritePosition (stdout, l_scan_Attribute.Position); printf (" ");
      switch (Token) {
         case tok_int_const:
            printf ("int_const    : %ld\n",l_scan_Attribute.int_const.Value);
            break;
         case tok_float_const:
            printf ("float_const    : %s\n",l_scan_Attribute.float_const.Value);
            break;
         case tok_string_const:
            printf ("string_const    : %s\n",l_scan_Attribute.string_const.Value);
            break;
         case tok_int:
            printf ("int\n");
            break;
         case tok_float:
            printf ("float\n");
            break;
         case tok_string:
            printf ("string\n");
            break;
         case tok_identifier:
            printf ("identifier    : %s\n",l_scan_Attribute.identifier.Value);
            break;
         case tok_operator:
            printf ("operator    : %s\n",l_scan_Attribute.operator.Value);
            break;
         case tok_comparator:
            printf ("comparator    : %s\n",l_scan_Attribute.comparator.Value);
            break;
         case tok_var_modifier:
            printf ("var_modifier    : %s\n",l_scan_Attribute.var_modifier.Value);
            break;
         case tok_openbracket:
            printf ("openbracket\n");
            break;
         case tok_closebracket:
            printf ("closebracket\n");
            break;
         case tok_for:
            printf ("for_loop\n");
            break;
         case tok_inrange:
            printf ("inrage\n");
            break;
         case tok_while:
            printf ("while_loop\n");
            break;
         case tok_if:
            printf ("if_loop\n");
            break;
         case tok_else:
            printf ("else\n");
            break;
         case tok_endif:
            printf ("endif\n");
            break;
         case tok_endfor:
            printf ("endfor\n");
            break;
         case tok_endwhile:
            printf ("endwhile\n");
            break;
         case tok_then:
            printf ("then\n");
            break;
         case tok_do:
            printf ("do\n");
            break;
         case tok_assignment:
            printf ("assignment\n");
            break;
         case tok_program:
            printf ("program\n");
            break;
         case tok_doppelpunkt:
            printf ("doppelpunkt\n");
            break;
         case tok_run:
            printf ("run\n");
            break;
         case tok_endprogram:
            printf ("endprogram\n");
            break;
         /*case tok_var_modifier:
            printf ("var_modifier    : %s\n",l_scan_Attribute.var_modifier_const.Value);
            break;*/
         default: fprintf (stderr, "FATAL ERROR, unknown token\n");
      }
   }
   printf ("Token count: %d\n", Count);
   return 0;
}
