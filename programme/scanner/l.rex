/* Project:  COCKTAIL training
 * Descr:    a simple scanner generated with rex
 * Kind:     REX scanner specification (solution)
 * Author:   Dr. Juergen Vollmer <juergen.vollmer@informatik-vollmer.de>
 * $Id: l.rex.in,v 1.18 2021/11/04 08:26:00 vollmer Exp $
 * 3 Schlüsselworte
 * 2 Operatoren
 * Kommentare einfügen
 * Konstanten: String, Float, Int
 * => komplettes Verzeichnis als gezipptes tar hochladen. Nachname-Nachname-Nachname-Nachname-Nachname
 * => KEINE WARNUNGEN ausser unused-function
 * => UNSERE SPRACHE X
 * => AKTUELLE SPRACHDEFINITION dazulegen -> liegt in sprache_x datei
 */

SCANNER l_scan

EXPORT {
/* code to be put into Scanner.h */

# include "Position.h"

/* Token Attributes.
 * For each token with user defined attributes, we need a typedef for the
 * token attributes.
 * The first struct-field must be of type tPosition!
 */
typedef struct {tPosition Pos; long Value;} tint_const;

typedef struct {tPosition Pos; char* Value;} tfloat_const;
typedef struct {tPosition Pos; char* Value;} tstring_const;
typedef struct {tPosition Pos; char* Value;} tvar_modifier;
typedef struct {tPosition Pos; char* Value;} tidentifier;
typedef struct {tPosition Pos; char* Value;} toperator;
typedef struct {tPosition Pos; char* Value;} tcomparator;

/* There is only one "actual" token, during scanning. Therfore
 * we use a UNION of all token-attributes as data type for that unique
 * token-attribute variable.
 * All token (with and without user defined attributes) have one
 * attribute: the source position:
 *     tPosition     Position;
 */
typedef union {
  tPosition     Position;
  tint_const    int_const;
  tfloat_const    float_const;
  tstring_const   string_const;
  tvar_modifier   var_modifier;
  tidentifier  identifier;
  toperator   operator;
  tcomparator   comparator;
} l_scan_tScanAttribute;

/* Tokens are coded as int's, with values >=0
 * The value 0 is reserved for the EofToken, which is defined automatically
 */
# define tok_int_const    1
# define tok_int 2
# define tok_float 3
# define tok_float_const 4
# define tok_string 5
# define tok_string_const 6
# define tok_identifier 7
# define tok_program 8
# define tok_program_doppelpunkt 9
# define tok_run 10
# define tok_endprogram 11
# define tok_var_modifier 12

# define tok_for 13
# define tok_inrange 14
# define tok_while 15
# define tok_if 16
# define tok_else 17
# define tok_endif 18
# define tok_endfor 19
# define tok_endwhile 20
# define tok_then 21
# define tok_do 22

# define tok_assignment 23
# define tok_doppelpunkt 24

# define tok_operator 25
# define tok_openbracket 26
# define tok_closebracket 27

# define tok_comparator 28

} // EXPORT

GLOBAL {
  # include <stdlib.h>
  # include "rString.h"
} // GLOBAL

LOCAL {
 /* user-defined local variables of the generated GetToken routine */
}  // LOCAL

DEFAULT {
  /* What happens if no scanner rule matches the input */
  MessageI ("Illegal character", xxError, l_scan_Attribute.Position, xxCharacter, l_scan_TokenPtr);
} // DEFAULT

EOF {
  /* What should be done if the end-of-input-file has been reached? */

  /* E.g.: check that strings and comments are closed. */
  switch (yyStartState) {
  case STD:
    /* ok */
    break;
  default:
    Message ("OOPS: that should not happen!!", xxFatal, l_scan_Attribute.Position);
    break;
  }

  /* implicit: return the EofToken */
} // EOF

DEFINE  /* some abbreviations */
  digit  = {0-9}           .
  lowerUpperChar  = {a-zA-Z}    .
  lowerChar  = {a-z}       .
  upperChar  = {A-Z}       .
  character  = {a-zA-Z0-9} .

/* define start states, note STD is defined by default, separate several states by a comma */

RULE

/* control structures */
#STD# "for" : {return tok_for;}
#STD# "inrange" : {return tok_inrange;}
#STD# "while" : {return tok_while;}
#STD# "if" : {return tok_if;}
#STD# "else" : {return tok_else;}
#STD# "endif" : {return tok_endif;}
#STD# "endfor" : {return tok_endfor;}
#STD# "endwhile" : {return tok_endwhile;}
#STD# "then" : {return tok_then;}
#STD# "do" : {return tok_do;}

#STD# ":=" : {return tok_assignment;}

/* program region seperators */
#STD# "program" : {return tok_program;}
#STD# ":" : {return tok_doppelpunkt;}
#STD# "run:" : {return tok_run;}
#STD# "endprogram" : {return tok_endprogram;}

/* variable modifiers */
#STD# ("in"|"out"|"inout") :
    {
      l_scan_Attribute.var_modifier.Value = malloc (l_scan_TokenLength+1);
      l_scan_GetWord (l_scan_Attribute.var_modifier.Value);
      return tok_var_modifier;
	}

/* Integers */
#STD# ("-")?digit+ :
	{
      l_scan_Attribute.int_const.Value = atol (l_scan_TokenPtr);
      return tok_int_const;
	}

/* Please add rules for: (don't forget to adapt main()) */
/* Float numbers */
#STD# "-"? digit+ "." digit+ :
    {
      l_scan_Attribute.float_const.Value = malloc (l_scan_TokenLength+1);
      l_scan_GetWord (l_scan_Attribute.float_const.Value);
      return tok_float_const;
	}

/* case insensitive keywords: BEGIN PROCEDURE END CASE */
/* not allowed: all keywords are lowercase -> no case insensitiveness */

/* keywords */
#STD# "int" :
{return tok_int;}

#STD# "float" :
{return tok_float;}

#STD# "string" :
{return tok_string;}

/* identifiers */
#STD# upperChar(character)* :
    {
      l_scan_Attribute.identifier.Value = malloc (l_scan_TokenLength+1);
      l_scan_GetWord (l_scan_Attribute.identifier.Value);
      return tok_identifier;
	}

/* comment up to end of line */
#STD# "#" ANY * : {/* ignore comments */}

/* C-style comment */
/* not allowed */

/* Modula2-style nested comment */
/* not allowed */

/* double 'quote' delimited strings */
#STD# "~"character*"~" :
    {
      l_scan_Attribute.string_const.Value = malloc (l_scan_TokenLength+1);
      l_scan_GetWord (l_scan_Attribute.string_const.Value);
      return tok_string_const;
	}

/* operator */
#STD# "+"|"-"|"*"|"/"|"%":
    {
      l_scan_Attribute.operator.Value = malloc (l_scan_TokenLength+1);
      l_scan_GetWord (l_scan_Attribute.operator.Value);
      return tok_operator;
	}

/* brackets */
#STD# "(" :
{return tok_openbracket;}

#STD# ")" :
{return tok_closebracket;}

/* comparator */
#STD# "<"|">"|"="|"!=":
    {
      l_scan_Attribute.comparator.Value = malloc (l_scan_TokenLength+1);
      l_scan_GetWord (l_scan_Attribute.comparator.Value);
      return tok_comparator;
	}

/**********************************************************************/
