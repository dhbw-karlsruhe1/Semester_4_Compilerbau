Aufgabe | Name | Punkte | Verzeichnis | Aufgabenstellung
:----- | :---- | :----: | -----: | -----:
Referat | Thema | 0.0/2.0 | [Referat/](Referat/) |  [Aufgabe Ref.](Referat/A0_Referat.md)
Aufgabe 1 | Sprache X: Sprachdefinition | 2.0/2.0 | [Aufgabe_1/](Aufgabe_1/)| [Aufgabe 1](Aufgabe_1/A1_Aufgabenstellung.md)
Aufgabe 2 | Gleitkommazahlen (reg. Grammatik) | 1.0/1.0 | [Aufgabe_2/](Aufgabe_2/) |  [Aufgabe 2](Aufgabe_2/A2_Aufgabenstellung.md)
Aufgabe 3 | Gleitkommazahlen (NEA/DEA) | 2.0/2.0 | [Aufgabe_3/](Aufgabe_3/) |  [Aufgabe 3](Aufgabe_3/A3_Aufgabenstellung.md)
Aufgabe 4 |  Sprache X: Scanner | 2.0/2.0 | [scanner/](scanner/) |  [Aufgabe 4](scanner/A4_Aufgabenstellung.md)
Aufgabe 5 | Rekursiver Abstiegsparser | 4.0/4.0 | [recursive-descent/](recursive-descent/) |  [Aufgabe 5](recursive-descent/A5_Aufgabenstellung.md)
Aufgabe 6 | Lark definition fertig stellen | Keine Abgabe/ Punkte | [Aufgabe_6/](Aufgabe_6/) |  [Aufgabe 6](Aufgabe_6/A6_Aufgabenstellung.md)
 | | | | + [lr-parser/](lr-parser/)
Aufgabe 7 | Sprache X: Parser | 1.0/2.0 | [Aufgabe_7/](Aufgabe_7/) | [Aufgabe 7.1](Aufgabe_7/A7.1_Aufgabenstellung.md)
 |  | Sprache X: AST | 2.0/2.0 | [Aufgabe_7/](Aufgabe_7/) | [Aufgabe 7.2](Aufgabe_7/A7.2_Aufgabenstellung.md)
Aufgabe 8 | Sprache X: Baumaufbau | 0.0/2.0 | [Aufgabe_8/](Aufgabe_8/) |  [Aufgabe 8](Aufgabe_8/A8_Aufgabenstellung.md)
||||
SUMME | | 14.0/19.0 | [./](./) | [./Aufgabe-Verzeichnis.md](./Aufgabe-Verzeichnis.md)
||||
\- | Punkte in der Klausur | CEIL(Punkte/4) | = CEIL(3.5) | = 4 Klausur Punkte

=> 4 von 5 extra Punkten in einer Klausur mit 100 Punkten\
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;=> War es den Zeitaufwand absolut gar nicht wert.

VLG. [./hausaufgaben-punkte.pdf](hausaufgaben-punkte.pdf)