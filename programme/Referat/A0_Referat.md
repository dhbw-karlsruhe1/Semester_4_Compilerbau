# Referat (2 Punkte)
Wenn Sie ein Referat halten, laden Sie bitte die Präsentation (als PDF) hier hoch. Bitte benutzen Sie als Dateinamen den Titel Ihres Vortrags.
Dauer des Vortrags: ca. 5-7 Minuten. Gruppenvorträge müssen von mir vorab genehmigt werden.
Die Vorträge werden i.d.R. in der letzten Vorlesung vor der Klausur gehalten gehalten. Punkte gibt es nur, wenn der Vortrag rechtzeitig hochgeladen und am gehalten wurde.

---

Feedback:



## Punkte:

0.0/2.0