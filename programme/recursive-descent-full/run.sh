#!/bin/bash

CMD=`basename $0`

DIR=/home/vollmer/ba/uebau/programme/recursive-decent-full
OK_FILE=/tmp/$CMD.$$.ok
ERR_FILE=/tmp/$CMD.$$.err

if [ $# -eq 0 ]
then
    DIRS=`ls .`
else
    DIRS=$1
fi


error()
{
    rm -f $OK_FILE
    echo "ERROR: $*" >&2
}

clean()
{
    (
	for d in $DIRS
	do
	    [ -d $d ] || continue
	    if [ -f $d/Makefile -a -f $d/parser.c ]
	    then
		echo ">>>> $d"
		make -C $d clean
		echo "--------------------------------------";echo;echo
	    fi
	done
    ) > /dev/null
}

compile()
{
    for d in $DIRS
    do
	[ -d $d ] || continue
	if [ -f $d/Makefile -a -f $d/parser.c ]
	then
	    echo ">>>> $d"
	    make -C $d || error "*** compiling $f failed ***"
	    echo "--------------------------------------";echo;echo
	fi
    done
}

run()
{
    for d in $DIRS
    do
	[ -d $d ] || continue
	if [ -x $d/parser ]
	then
	    touch $OK_FILE
	    rm -f $ERR_FILE
	    {
		echo "--------------------------------------";
		echo ">>>> $d"

		for f in $DIR/test-*
		do
		    echo -n "..... $f "
		    $d/parser $f  || error "*** $f should be ok ***"
		done
		for f in $DIR/err-test-*
		do
		    echo -n "..... $f "
		    $d/parser $f  && error "*** $f should have an error ***"
		done
		echo
	    } > $ERR_FILE  2>&1
	    if [ -f $OK_FILE ]
	    then
		printf " ---> %56s: alles ok  <----\n" $d
	    else
		#echo;echo;echo
		printf " ---> ERROR %50s: ERROR     <----\n" $d
		#cat $ERR_FILE
		#echo;echo;echo
	    fi
	fi
    done
    echo
}

clean
compile
run
clean
