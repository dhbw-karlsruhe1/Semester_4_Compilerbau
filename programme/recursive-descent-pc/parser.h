/* Project:  COCKTAIL training
 * Descr:    A recursive Decent Parser for expressions
 * Kind:     The parser interface
 * Author:   Dr. Juergen Vollmer <juergen.vollmer@informatik-vollmer.de>
 * $Id: parser.h,v 1.1 2007/05/31 15:48:42 vollmer draft $
 */

# ifndef parser_H
# define parser_H

# include "expr_scan.h"
# define NoAttribute ((expr_scan_tScanAttribute*)0)

typedef enum {
  false = 0,
  true  = 1
} bool;

extern tToken CurToken;
/* current analyzed token */

extern bool match (tToken token, expr_scan_tScanAttribute *attribute);
/* returns true, iff CurToken == token, and reads next token
 * sets attribute to the value expr_scan_Attribute.
 */

extern bool f_E (double *result);
/* `E' is the root symbol of the grammer.
 * The function 'f_E()' returns true, iff 'CurToken' starts a sequence of
 * input-tokens which may be generated by 'E'.
 * `CurToken' refers to then to the next token.
 * 'result' holds the computed value.
*/

# endif
