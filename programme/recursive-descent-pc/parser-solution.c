/* Project:  COCKTAIL training
 * Descr:    A recursive Decent Parser for expressions
 * Kind:     The parser (solution)
 * Author:   Dr. Juergen Vollmer <juergen.vollmer@informatik-vollmer.de>
 * $Id: parser.c.in,v 1.4 2013/05/06 12:09:14 vollmer Exp $
 */

# include <stdlib.h>
# include "expr_scan.h"
# include "parser.h"
# include "Errors.h"
# include "string.h"
      	/* contains: `Message', `MessageI', `xxFatal', `xxError' */

tToken CurToken;

/*****************************************************************************
 * helpers
 *****************************************************************************/

/* Debugging:  if the C-compiler is called with `-DDEBUG',
 *             some output will be generated
 */
# ifdef DEBUG
# define DEBUG_show(msg) printf ("%-5s cur = `%s'\n", msg, token2string (CurToken));
# else
# define DEBUG_show(msg)  /* no output */
# endif

static const char* token2string (tToken token)
/* maps token-code to token textual representation */
{
  switch (token) {
  case '+':
    return "+";
  case '-':
    return "-";
  case '*':
    return "*";
  case '/':
    return "/";
  case '(':
    return "(";
  case ')':
    return ")";
  case tok_identifier:
    return "identifier";
  case tok_float_const:
    return "float_const";
  case expr_scan_EofToken:
    return "EOF";
  default:
    /* this should never happen */
    MessageI ("FATAL ERROR: unknown token", xxFatal,
	      expr_scan_Attribute.Position, xxInteger, (char*) &token);
    abort(); /* aborts the program */
  }
}

/******************************************************************************/

bool match (tToken token, expr_scan_tScanAttribute *attribute)
/* Instead of writing for each token 't' a function 'f_t()', we use
 * 'match (t)'.
 * if attribute != NoAttribute, store expr_scan_Attribute in attribute.
 */
{
  if (attribute != NoAttribute) *attribute = expr_scan_Attribute;
  DEBUG_show ("MATCH");
# ifdef DEBUG
  printf ("      cur = `%s' expected = `%s'\n", token2string(CurToken), token2string (token));
# endif
  if (CurToken == token) {
    if (CurToken != expr_scan_EofToken) CurToken = expr_scan_GetToken();
    DEBUG_show ("  new");
    return true;
  } else {
    char msg[255];
    sprintf (msg, "Syntax Error: found: `%s' expected: `%s'",
	     token2string (CurToken), token2string (token));
    Message (msg, xxError, expr_scan_Attribute.Position);
    return false;  /* Syntax Error */
  }
}

/*****************************************************************************/

double *eval(double *result, char op, double arg)
/* returns     *result op= arg
 * where op is one of '+', '-', '*', '/'
 * division by zero is checked.
 * returns result (note the pointer!)
 */
{
  switch (op) {
  case '+': *result += arg; return result;
  case '-': *result -= arg; return result;
  case '*': *result *= arg; return result;
  case '/':
    if (arg == 0) {
      Message ("division by zero", xxError, expr_scan_Attribute.Position);
      *result = 0;
    } else {
      *result /= arg;
    }
    return result;
  default:
    Message ("eval(..), illegal operand", xxFatal, expr_scan_Attribute.Position);
    abort();
  }
}

/*****************************************************************************
 * First-Follow-set Tests
 * 1)  E  ::= T E1         FIRST (T E1    FOLLOW (E))  = { ( id fc   }
 * 2)  E1 ::= + T E1       FIRST (+ T E1  FOLLOW (E1)) = { +         }
 * 2a) E1 ::= - T E1       FIRST (- T E1  FOLLOW (E1)) = { -         }
 * 3)  E1 ::= epsilon      FIRST (epsilon FOLLOW (E1)) = { eof )     }
 * 4)  T  ::= F T1         FIRST (F T1    FOLLOW (T))  = { ( id fc   }
 * 5)  T1 ::= * F T1       FIRST (* F T1  FOLLOW (T1)) = { *         }
 * 5a) T1 ::= / F T1       FIRST (/ F T1  FOLLOW (T1)) = { /         }
 * 6)  T1 ::= epsilon      FIRST (epsilon FOLLOW (T1)) = { eof ) + - }
 * 7)  F  ::= ( E )        FIRST ( ( E )  FOLLOW (F))  = { (         }
 * 8)  F  ::= id           FIRST (id      FOLLOW (F))  = { id        }
 * 9)  F  ::= fc           FIRST (fc      FOLLOW (F))  = { fc        }
 *
 * The function 'FirstFollow_i ()' returns true, if and only if 'CurToken'
 * is element of the appropriate set.
 ******************************************************************************/

static bool FirstFollow_1 ()
{
  switch (CurToken) {
  case '(': case tok_identifier: case tok_float_const:
    return true;
  default:
    return false;
  }
}

static bool FirstFollow_2 (void)
{
  switch (CurToken) {
  case '+':
    return true;
  default:
    return false;
  }
}

static bool FirstFollow_2a (void)
{
  switch (CurToken) {
  case '-':
    return true;
  default:
    return false;
  }
}

static bool FirstFollow_3 (void)
{
  switch (CurToken) {
  case expr_scan_EofToken: case ')':
    return true;
  default:
    return false;
  }
}
static bool FirstFollow_4 (void)
{
  switch (CurToken) {
  case '(': case tok_identifier: case tok_float_const:
    return true;
  default:
    return false;
  }
}
static bool FirstFollow_5 (void)
{
  switch (CurToken) {
  case '*':
    return true;
  default:
    return false;
  }
}
static bool FirstFollow_5a (void)
{
  switch (CurToken) {
  case '/':
    return true;
  default:
    return false;
  }
}

static bool FirstFollow_6 (void)
{
  switch (CurToken) {
  case expr_scan_EofToken: case ')': case '+': case '-':
    return true;
  default:
    return false;
  }
}
static bool FirstFollow_7 (void)
{
  switch (CurToken) {
  case '(':
    return true;
  default:
    return false;
  }
}
static bool FirstFollow_8 (void)
{
  switch (CurToken) {
  case tok_identifier:
    return true;
  default:
    return false;
  }
}
static bool FirstFollow_9 (void)
{
  switch (CurToken) {
  case tok_float_const:
    return true;
  default:
    return false;
  }
}

/******************************************************************************
 * We use the following grammar for arithmetic expressions:
 * 1)  E  ::= T E1
 * 2)  E1 ::= + T E1       2a) E1 ::= - T E1
 * 3)  E1 ::= epsilon
 * 4)  T  ::= F T1
 * 5)  T1 ::= * F T1       5a)  T1 ::= / F T1
 * 6)  T1 ::= epsilon
 * 7)  F  ::= ( E )        8)  F  ::= id       9) F ::= float_const
 ******************************************************************************/

/* The function 'f_nt()' for the non-terminal 'nt' returns true, iff
 * 'CurToken' starts a sequence of input-tokens which may be generated by 'nt'.
 * 'CurToken' refers after that to the next token to be processed.
 * Note: eof = end-of-file  is a token too.
 */

/* forward-declarations of nonterminal-functions, needed by the langauge C
 * f_expr () is already declared in parser.h
 */
static bool f_E1 (double *result);
static bool f_T  (double *result);
static bool f_T1 (double *result);
static bool f_F  (double *result);


bool f_E (double *result)
{
  DEBUG_show("E");

  if (FirstFollow_1()) return f_T(result) && f_E1(result);
  Message ("Syntax Error in <E>", xxError, expr_scan_Attribute.Position);
  return false; /* Syntax Error */
}

bool f_E1 (double *result)
{
  double arg;
  DEBUG_show("E1");

  /* E1 ::= + T E1 */
  if (FirstFollow_2())  return match ('+', NoAttribute) && f_T(&arg) && f_E1(eval (result, '+', arg));

  /* E1 ::= - T E1 */
  if (FirstFollow_2a()) return match ('-', NoAttribute) && f_T(&arg) && f_E1(eval (result, '-', arg));

  /* E1 ::= epsilon */
  if (FirstFollow_3()) return true;
  Message ("Syntax Error in <E1>", xxError, expr_scan_Attribute.Position);
  return false; /* Syntax Error */
}

bool f_T (double *result)
{
  DEBUG_show("T");

  if (FirstFollow_4()) return f_F(result) && f_T1(result);
  Message ("Syntax Error in <T>", xxError, expr_scan_Attribute.Position);
  return false; /* Syntax Error */
}

bool f_T1 (double *result)
{
  double arg;

  DEBUG_show("T1");

  /* T1 ::= * F T1 */
  if (FirstFollow_5()) return match ('*', NoAttribute) && f_F(&arg) && f_T1(eval (result, '*', arg));

  /* T1 ::= / F T1 */
  if (FirstFollow_5a()) return match ('/', NoAttribute) && f_F(&arg) && f_T1(eval (result, '/', arg));

  /* T1 ::= epsilon */
  if (FirstFollow_6()) return true;
  Message ("Syntax Error in <T1>", xxError, expr_scan_Attribute.Position);
  return false; /* Syntax Error */
}

bool f_F (double *result)
{
  expr_scan_tScanAttribute attribute;

  DEBUG_show("F");

  if (FirstFollow_7()) return match ('(', NoAttribute) && f_E(result) && match (')', NoAttribute);
  if (FirstFollow_8()) {
    if (match (tok_identifier, &attribute)) {
      if (strcmp (attribute.identifier.Value, "pi") == 0) {
	*result = 3.141592653;
	return true;
      } else if (strcmp (attribute.identifier.Value, "e") == 0) {
	*result = 2.71828;
	return true;
      } else {
	Message ("only the identifier 'pi' and 'e' are allowed", xxError, expr_scan_Attribute.Position);
	return false;
      }
    }
  }
  if (FirstFollow_9()) {
    if (match (tok_float_const, &attribute)) {
      *result = attribute.float_const.Value;
      return true;
    }
  }
  Message ("Syntax Error in <F>", xxError, expr_scan_Attribute.Position);
  return false; /* Syntax Error */
}

/***********************  E  N  D  ***********************************************/
