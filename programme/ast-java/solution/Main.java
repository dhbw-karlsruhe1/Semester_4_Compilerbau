/* Project:  Vorlesung Uebersetzerbau
 * Descr:    Aufbau AST in JAVA
 * Kind:     Hauptprogramm
 * Author:   Prof. Dr. Juergen Vollmer <vollmer@dhbw-karlsruhe.de>
 * $Id: Main.java,v 1.2 2020/07/05 13:33:39 vollmer Exp $
 */
public class Main
{
    public static void main(String argv[])
    {
	System.out.println(">>>>>>> Construction of an AST for expressions <<<<<<<<");
	EXPR tree1 =
	     new BinaryExpr (
	 	 '+',
		 new IntConst (1),
		 new BinaryExpr (
		     '*',
		     new UnaryExpr (
                         '-',
		 	 new IntConst (2)),
		     new IntConst (3)));
	tree1.write();
	System.out.println(" ==> " + tree1.eval());

	EXPR tree2 =
             new CondExpr (
	         new BinaryExpr (
	 	     '<',
		     new IntConst (2),
		     new IntConst (3)),
                 new IntConst (2),
                 new IntConst (3));
	tree2.write();
	System.out.println(" ==> " + tree2.eval());

	System.out.println(">>>>>>> List of expressions <<<<<<<<");
	EXPRS list =
	      new Exprs (
		  new Exprs (
		      new NoExprs (),
		      tree2),
                  tree1);
	list.write_and_eval();

	System.out.println(">>>>>>> Reverted list of expressions <<<<<<<<");
	EXPRS r = list.reverse();
	r.write_and_eval();

	System.out.println(">>>>>>> Interpreter <<<<<<<<");
	Storage storage = Storage.getInstance();
	// Storage.write();     // calls with class name
	// Storage.set ("A", 1);
	// storage.get ("C");  // -> not declared
	//  storage.write();   // call with object

	STMT s1 = new AssignStmt (new Name ("A"), new IntConst (1));
	STMT s2 = new AssignStmt (new Name ("B"), new IntConst (2));
	STMT s3 = new AssignStmt (new Name ("C"), new IntConst (3));
	STMT s4 = new AssignStmt (new Name ("D"),
				  new BinaryExpr (
			  	      '+',
				      new Name ("A"),
				      new BinaryExpr (
					  '*',
					  new Name("B"),
					  new Name("C"))));
	s1.Next = s2;
	s2.Next = s3;
	s3.Next = s4;
	s1.write();
	s1.interprete();
	Storage.write();
    }
}
