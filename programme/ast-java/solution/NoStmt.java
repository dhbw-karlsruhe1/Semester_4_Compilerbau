/* Project:  Vorlesung Uebersetzerbau
 * Descr:    Aufbau AST in JAVA
 * Kind:     Abstrakte Klasse EXPR
 * Author:   Prof. Dr. Juergen Vollmer <vollmer@dhbw-karlsruhe.de>
 * $Id: NoStmt.java,v 1.1 2020/07/05 13:30:14 vollmer draft $
 */
class NoStmt extends STMTS
{
    public NoStmt ()
    {
	// nothing
    }

    public void interprete()
    {
	// nothing
    }
    public void write()
    {
	// nothing
    }
}
