/* Project:  Vorlesung Uebersetzerbau
 * Descr:    Aufbau AST in JAVA
 * Kind:     Klasse fuer IntConst-Knoten
 * Author:   Prof. Dr. Juergen Vollmer <vollmer@dhbw-karlsruhe.de>
 * $Id: IntConst.java,v 1.1 2020/07/05 13:30:14 vollmer draft $
 */
class IntConst extends EXPR
{
    public int Value;
    public IntConst (int _Value)
    {
	Value = _Value;
    }

    public void write()
    {
	System.out.print(Value);
    }

    public double eval()
    {
	return Value;
    }
}