/* Project:  Vorlesung Uebersetzerbau
 * Descr:    Aufbau AST in JAVA
 * Kind:     Abstrakte Klasse EXPR
 * Author:   Prof. Dr. Juergen Vollmer <vollmer@dhbw-karlsruhe.de>
 * $Id: STMTS.java,v 1.1 2020/07/05 13:30:14 vollmer draft $
 */
abstract class STMTS
{
    public abstract void interprete();
    public abstract void write();
}
