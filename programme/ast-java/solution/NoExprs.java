/* Project:  Vorlesung Uebersetzerbau
 * Descr:    Aufbau AST in JAVA
 * Kind:     Liste von Ausdruecken: Listen-Ende-Knoten
 * Author:   Prof. Dr. Juergen Vollmer <vollmer@dhbw-karlsruhe.de>
 * $Id: NoExprs.java,v 1.1 2020/07/05 13:30:14 vollmer draft $
 */
class NoExprs extends EXPRS
{
    public NoExprs ()
    {
	// Nothing to do
    }
    public void write_and_eval ()
    {
	// Nothing to do
    }
}
