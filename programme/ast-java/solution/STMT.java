/* Project:  Vorlesung Uebersetzerbau
 * Descr:    Aufbau AST in JAVA
 * Kind:     Abstrakte Klasse EXPR
 * Author:   Prof. Dr. Juergen Vollmer <vollmer@dhbw-karlsruhe.de>
 * $Id: STMT.java,v 1.1 2020/07/05 13:30:14 vollmer draft $
 */
abstract class STMT extends STMTS
{
    public STMTS Next;
    public STMT (STMTS _Next)
    {
	Next = _Next;
    }
}
