/* Project:  Vorlesung Uebersetzerbau
 * Descr:    Aufbau AST in JAVA
 * Kind:     Klasse fuer UnaryExpr-Knoten
 * Author:   Prof. Dr. Juergen Vollmer <vollmer@dhbw-karlsruhe.de>
 * $Id: UnaryExpr.java,v 1.1 2020/07/05 13:30:14 vollmer draft $
 */
class UnaryExpr extends EXPR
{
    public char op;
    public EXPR arg;
    public UnaryExpr (char _op, EXPR _arg)
    {
	op    = _op;
	arg   = _arg;
    }

    public void write()
    {
	System.out.print("(");
	System.out.print(op);
	arg.write();
	System.out.print(")");
    }

    public double eval()
    {
	switch (op) {
	case '-': return - arg.eval();
	case '+': return   arg.eval();
	default:  System.out.println("ERROR: UnaryExpr.eval(op=<"+op+"> undefined");
	          java.lang.System.exit(1);
		  return 0;
	}
    }

}
