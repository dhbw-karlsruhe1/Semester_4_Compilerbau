# Gleitkommazahlen (NEA/DEA) (2 Punkte)
Gegeben Ihre  rechtsreguläre Grammatik, mit welchen Sie "vereinfachten Gleitkommazahlen" darstellen können (siehe letzte Aufgabe).

- Erstellen Sie mit dem in der Vorlesung vorgestellten Algorithmus den endlichen Automaten. Zeichen Sie den resultierenden  Automaten als Graphen  und markieren Sie alle nicht-deterministischen Übergänge.
- Wenden Sie die Teilmengenkonstruktion an, um den Automaten deterministisch zu machen. Zeichnen Sie den resultierenden Automaten.

Bitte geben Sie Ihre rechtsreguläre Grammatik in der Lösung an!

---

## Kommentar:
Fehlende Ableitungen von Hausaufgabe 1 Aufgabe 2 (Gleitkommazahlen) mit im PDF

---

## Feedback:

> ok

## Punkte:

2,00/2,00