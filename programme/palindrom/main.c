/* Project:  COCKTAIL training
 * Descr:    A simple pocket computer (scanner, parser, evaluator)
 * Kind:     C-main program
 * Author:   Prof. Dr. Juergen Vollmer <vollmer@dhbw-karlsruhe.de>
 * $Id: main.c,v 1.2 2018/11/12 10:40:24 vollmer Exp $
 */

# include <stdio.h>
# include <stdlib.h>
# include <string.h>
# include "Position.h"
# include "Errors.h"
# include "palindrom_scan.h"
# include "palindrom_pars.h"

int main (int argc, char *argv[])
{
   int errors = 0;
   if (argc == 2) {
     if (strcmp (argv[1], "-h") == 0) {
       fprintf (stderr,
		"usage: %s [-h] [file]\n"
		"  palindrom LR based parser, which read stdin or file and checks a word over the terminal alphabet {a,b} and {1,2,3}\n"
		"  -h: Help\n", argv[0]);
       exit (0);
     }
   }

   if (argc == 2) {
     palindrom_scan_Attribute.Position.FileName = MakeIdent1 (argv[1]);
     palindrom_scan_BeginFile (argv[1]);
     /* Read from file argv[1].
      * If this routine is not called, stdin is read.
      */
   }


   palindrom_pars_Debug = rtrue;

   /* PrintMessages(rfalse);  * Syntaxfehler nicht ausgeben */
   errors = palindrom_pars ();  /* the parser */

   printf ("\t number of errors: %d ", errors);

   return (errors == 0)? 0 : 1;
}
