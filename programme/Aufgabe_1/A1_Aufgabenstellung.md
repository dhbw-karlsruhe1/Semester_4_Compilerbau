# Sprache X: Sprachdefinition (2 Punkte)
## Sprachbeschreibung von X

Spezifizieren Sie möglichst genau die Programmiersprache X:

Programme in X sollen einen Namen bekommen, sowie einen Variablendeklarations- und einen Ausführungsteil haben.
- Variablen müssen deklariert werden und sind entweder vom Typ int, float oder string.
- Variablen können so ausgezeichnet werden, dass Ihr Wert am Anfang abgefragt bzw. Ende ausgegeben wird.
- Im Ausführungsteil sind Zuweisungen von mathematischen Ausdrücken an Variablen erlaubt, diese erlauben die vier Grundrechenarten und folgen den üblichen Konventionen.
- Im Ausführungsteil sind Zuweisungen an Strings erlaubt. Auf der rechten Seite stehen dabei entweder Variablen oder Konkatenationen von Strings mit dem Plus-Operator.
- Im Ausführungsteil sind Bedingte Anweisungen (if-then-else) sowie for- und while-Schleifen zugelassen.
- Für Bedingungen stehen die üblichen Vergleichsoperatoren zur Verfügung.
- Ausführungsblöcke werden mit begin und end geklammert.

Aufgaben

- Erstellen Sie eine Grammatik in EBNF.
- Ergänzen Sie die Grammatik um die nötigen weiteren Angaben und Spezifikationen um eine eindeutige und sinnvolle Sprachdefinition zu bekommen.
- Wo nutzen Sie explizit oder implizit geäußertes "Allgemeinwissen"?
- Schreiben Sie ein Programm in X, welches eine ganze Zahl n "einliest" und n! ausgibt.
- Schreiben Sie ein Programm in X, welches den GGT zweier ganzen Zahlen bestimmt.
- Da Sie die Sprachdefinition ggf. "nachbessern" müssen, erstellen Sie sie mit einem Textverarbeitungsprogramm und laden die PDF hoch. (Sie müssen das also nicht handschriftlich machen)

---

## Feedback:
> Schauen Sie sich mal an, wie andere Sprachen mit Leerzeichen umgehen. Ist Ihre Grammatik "lesbar", so wie Sie mit Leerzeichen ...

## Punkte:

2,00/2,00