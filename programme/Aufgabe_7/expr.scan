/* Project:  COCKTAIL training
 * Descr:    a simple scanner generated with rex
 * Kind:     REX scanner specification (solution)
 * Author:   Dr. Juergen Vollmer <juergen.vollmer@informatik-vollmer.de>
 * $Id: l.rex.in,v 1.18 2021/11/04 08:26:00 vollmer Exp $
 * 3 Schlüsselworte
 * 2 Operatoren
 * Kommentare einfügen
 * Konstanten: String, Float, Int
 * => komplettes Verzeichnis als gezipptes tar hochladen. Nachname-Nachname-Nachname-Nachname-Nachname
 * => KEINE WARNUNGEN ausser unused-function
 * => UNSERE SPRACHE X
 * => AKTUELLE SPRACHDEFINITION dazulegen -> liegt in sprache_x datei
 */

SCANNER expr_scan

EXPORT {
/* code to be put into Scanner.h */

  # include "Position.h"
  INSERT tScanAttribute

} // EXPORT



GLOBAL {
  # include <stdlib.h>
  # include "rString.h"
  # include "Errors.h"
  INSERT ErrorAttribute
} // GLOBAL

LOCAL {
 /* user-defined local variables of the generated GetToken routine */
}  // LOCAL

DEFAULT {
  /* What happens if no scanner rule matches the input */
  MessageI ("Illegal character", xxError, expr_scan_Attribute.Position, xxCharacter, expr_scan_TokenPtr);
} // DEFAULT

EOF {
  /* What should be done if the end-of-input-file has been reached? */

  /* E.g.: check that strings and comments are closed. */
  switch (yyStartState) {
  case STD:
    /* ok */
    break;
  default:
    Message ("OOPS: that should not happen!!", xxFatal, expr_scan_Attribute.Position);
    break;
  }

  /* implicit: return the EofToken */
} // EOF

DEFINE  /* some abbreviations */
  digit  = {0-9}           .
  lowerUpperChar  = {a-zA-Z}    .
  lowerChar  = {a-z}       .
  upperChar  = {A-Z}       .
  character  = {a-zA-Z0-9} .
  stringCharacter  = {a-zA-Z0-9 _+-*/%?!.,<>=$€&()[]#:"} .

/* define start states, note STD is defined by default, separate several states by a comma */

RULES

/* Integers */
#STD# ("-")?digit+ :
	{
      expr_scan_Attribute.int_const.Value = atol (expr_scan_TokenPtr);
      return int_const;
	}

/* Float numbers */
#STD# "-"? digit+ "." digit+ :
    {
      expr_scan_Attribute.float_const.Value = atof(expr_scan_TokenPtr);
      return float_const;
	}

/* identifiers */
#STD# upperChar(character)* :
    {
      expr_scan_Attribute.identifier.Value = MakeIdent(expr_scan_TokenPtr, expr_scan_TokenLength); 
      return identifier;
	}

/* comment up to end of line */
#STD# "#" ANY * : {/* ignore comments */}

/* C-style comment */
/* not allowed */

/* Modula2-style nested comment */
/* not allowed */

/* double 'quote' delimited strings */
#STD# "~"stringCharacter*"~" :
    {
      expr_scan_Attribute.string_const.Value = malloc (expr_scan_TokenLength+1);
      expr_scan_GetWord (expr_scan_Attribute.string_const.Value);
      return string_const;
	}

INSERT RULES #STD#
/**********************************************************************/
