# Sprache X: Baumaufbau (2 Punkte)
Definieren und implementieren Sie den AST für Ihre Sprache X.

- Spezifizieren graphisch (mit der in der Vorlesung gezeigten Notation) die Knotentypen. (Haben Sie ja bereits gemacht)
- Korrigieren Sie Ihre graphische Spezifikation und nutzen Sie von mir gemachten Anmerkungen. Inbes. schauen Sie sich nochmals "Ausdrücke" an. Auch Anweisungen wie if und while sind ja im Script angegeben.
- Implementieren Sie mit dem Werkzeug AST diese Spezifikation. Schauen Sie sich die Musterlösungen in den Verzeichnissen "expr-ast" und "expr-stmt" an. Die dort gezeigt Syntax sollte ausreichen Ihren AST zu implementieren.
- Implementieren Sie den Aufbau des ASTs in Ihrem Parser. Das bedeutet: Definieren der Nichtterminal-Attribute und Berechnungsregeln. Schauen Sie sich die Musterlösungs-Parser in obigen beiden Verzeichnissen an.
- Die main()-Funktion soll nach dem parsen der Eingabe die Funktion DrawTree() aufrufen.

Erstellen Sie die .ast -Datei und passen Sie die .pars-Datei an. Bringen Sie alles "fertig editiert" in die nächste Vorlesung (Di. 21.6.) mit. "Mutige" können die Makefile-Regeln für den Aufruf von AST aus dem expr-stmt in Ihr Makefile einbauen und versuchen das Ganze "zum Laufen zu bringen". Achtung das war für "Mutige" - alle anderen bringen die Dateien mit zur Vorlesung, dort gibt's dann Zeit dafür - und Sie können mich direkt fragen.

Abgabe des laufenden Parsers mit Baumaufbau ist dann am 22.6.

Bitte erstellen Sie wie in der letzten Aufgabe (Sprache X: Parser) ein entsprechendes tar-Archiv.  Legen Sie dem Verzeichnis die Sprachdefinition und die graphische Spezifikation des AST's bei.

---

## Kommentar:

Basierend auf [../Aufgabe_7/](../Aufgabe_7/)

---

## Feedback:
> Das übersetzt ja gar nicht.... da sind reichlich Fehler in expr.pars drin. Die AST-Spech ist auch nicht richtig, siehe z.B. die Zeile Range: (int_const | tIdent) !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! ----- daher 0 Pkte
## Punkte:
0.0/2.0