/* Project:  COCKTAIL training
 * Descr:    A recursive Decent Parser for expressions
 * Kind:     The parser (solution)
 * Author:   Prof. Dr. Juergen Vollmer <juergen.vollmer@dhbw-karlsruhe.de>
 * $Id: parser.c.in,v 1.7 2014/11/27 08:49:30 vollmer Exp $
 */

# include <stdlib.h>
# include <string.h>
# include "expr_scan.h"
# include "parser.h"
# include "Errors.h"
      	/* contains: `Message', `MessageI', `xxFatal', `xxError' */

tToken CurToken;

/******************************************************************************/

static const char* token2string (tToken token)
/* maps token-code to token textual representation */
{
  switch (token) {
  case '+':    return "+";
  case '*':    return "*";
  case '(':    return "(";
  case ')':    return ")";
  case '[':    return "[";
  case ']':    return "]";
  case '.':    return ".";
  case tok_identifier:
    return "identifier";
  case tok_arrow:
    return "->";
  case expr_scan_EofToken:
    return "EOF";
  default:
    /* this should never happen */
    MessageI ("FATAL ERROR: unknown token", xxFatal,
	      expr_scan_Attribute.Position, xxInteger, (char*) &token);
    abort(); /* aborts the program */
  }
}

/*****************************************************************************/

bool match (tToken token)
/* Instead of writing for each token 't' a function 'f_t()', we use 'match (t)'. */
{
# ifdef DEBUG
  printf ("      cur = `%s' expected = `%s'\n", token2string(CurToken), token2string (token));
# endif
  if (CurToken == token) {
    if (CurToken != expr_scan_EofToken) CurToken = expr_scan_GetToken();
    return true;
  } else {
    char msg[255];
    sprintf (msg, "Syntax Error: found: `%s' expected: `%s'",
	     token2string (CurToken), token2string (token));
    Message (msg, xxError, expr_scan_Attribute.Position);
    return false;  /* Syntax Error */
  }
}

/*****************************************************************************
 * Given the following grammar:
 *   E ::= E "+" T   | T
 *   T ::= T "*" F   | F
 *   F ::= "(" E ")" | D
 *   D ::= D "." identifier | D "[" E "]" | D "->" identifier | identifier
 * Implement a recurisve decent parser.
 ******************************************************************************/



/*****************************************************************************
 * That grammar is left-recursive, and therefore not LL(1), transform the garmmar:
 * epsilon is denoted as:    <e>
 * identifier is denoted as: i
 *
 *     Transformed Grammar        First(<X>)      Follow(<X>)      First (<alpha> Follow (<X>))
 *    <X> ::= <alpha>
 *  0) E  ::= T E'                { ( i }         { ) ] # }          { ( i }
 *  -------------------------------------------------------------------------------------------
 *  1) E' ::= + T E'              { + <e> }       { ) ] # }          { + }
 *  2)     |  <epsilon>                                              { ) ] # }
 *  -------------------------------------------------------------------------------------------
 *  3) T  ::= F T'                { ( i }         { + ) ] # }        { ( i }
 *  -------------------------------------------------------------------------------------------
 *  4) T' ::= * F T'              { * <e> }       { + ) ] # }        { * }
 *  5)     |  <e>                                                    {  + ) ] # }
 *  -------------------------------------------------------------------------------------------
 *  6) F  ::= ( E )               { ( i }         { * + ) ] # }      { ( }
 *  7)     |  D                                                      { i }
 *  -------------------------------------------------------------------------------------------
 *  8) D  ::= identifier D'       { i }           { * + ) ] # }      { i }
 *  -------------------------------------------------------------------------------------------
 *  9) D' ::= . identifier D'     { . [ -> <e> }  { * + ) ] # }      { . }
 * 10)     |  [ E ] D'                                               { [ }
 * 11)     |  -> identifier D'                                       { -> }
 * 12)     |  <e>                                                    { * + ) ] # }
 *  -------------------------------------------------------------------------------------------
 *
 ******************************************************************************/

# define RULE_COUNT    13
# define MAX_SET_ELEMS 5

const int first_follow_sets[RULE_COUNT][MAX_SET_ELEMS+1] =
  {
   /* Rule number / First (<alpha> Follow (<X>));   -1 denotes "no more elements in the set"  */
   /*  (0) */     {'(', tok_identifier, -1},
   /*  (1) */     {'+', -1},
   /*  (2) */     {')', ']', expr_scan_EofToken, -1},
   /*  (3) */     {'(', tok_identifier, -1},
   /*  (4) */     {'*', -1},
   /*  (5) */     {'+', ')', ']', expr_scan_EofToken, -1},
   /*  (6) */     {'(', -1},
   /*  (7) */     {tok_identifier, -1},
   /*  (8) */     {tok_identifier, -1},
   /*  (9) */     {'.', -1},
   /* (10) */     {'[', -1},
   /* (11) */     {tok_arrow, -1},
   /* (12) */     {'*', '+', ')', ']', expr_scan_EofToken, -1},
   };

bool ff_ok (int rule_number)
/* returns true, if the current token is in the FirstFollow-set of the given rule_numer, a value [0..RULE_COUNT-1] */
{
  int i;
# ifdef DEBUG
  printf ("ff_ok: rule=%d cur=%s\n", rule_number, token2string (CurToken));
# endif
  for (i = 0; first_follow_sets[rule_number][i] >= 0; i++) {
# ifdef DEBUG
    printf ("   i=%d ff=%s\n", i, token2string (first_follow_sets[rule_number][i]));
# endif
    if (CurToken == first_follow_sets[rule_number][i]) return true;
  }
  return false;
}

static const char *ff2string(int rn1, int rn2, int rn3, int rn4)
{
  static char str[255];
  str[0] = '\0';
  if (rn1 >= 0) {
    int i = 0;
    for (i = 0; first_follow_sets[rn1][i] >= 0; i++) {
      strcat (str, token2string (first_follow_sets[rn1][i]));
      strcat (str, " ");
    }
  }

  if (rn2 >= 0) {
    int i = 0;
    for (i = 0; first_follow_sets[rn2][i] >= 0; i++) {
      strcat (str, token2string (first_follow_sets[rn2][i]));
      strcat (str, " ");
    }
  }

  if (rn3 >= 0) {
    int i = 0;
    for (i = 0; first_follow_sets[rn3][i] >= 0; i++) {
      strcat (str, token2string (first_follow_sets[rn3][i]));
      strcat (str, " ");
    }
  }

  if (rn4 >= 0) {
    int i = 0;
    for (i = 0; first_follow_sets[rn4][i] >= 0; i++) {
      strcat (str, token2string (first_follow_sets[rn4][i]));
      strcat (str, " ");
    }
  }
  return str;
}

/******************************************************************************/

static void syntax_error(int rn1, int rn2, int rn3, int rn4)
{
  char msg[255];
  sprintf (msg, "Syntax Error: found: `%s' expected: `%s'",
	   token2string (CurToken), ff2string (rn1, rn2, rn3, rn4));
  Message (msg, xxError, expr_scan_Attribute.Position);
}

/******************************************************************************/

static bool f_E1(void);
static bool f_T(void);
static bool f_T1(void);
static bool f_F(void);
static bool f_D(void);
static bool f_D1(void);

bool f_E(void)
{
  /* 0) E  ::= T E' */
  if (ff_ok (0)) {
    return f_T() && f_E1();
  }

  syntax_error (0,-1,-1,-1);
  return false; /* Syntax Error */
}

static bool f_E1(void)
{
  /* 1) E' ::= + T E' */
  if (ff_ok (1)) {
    return match ('+') && f_T() && f_E1();
  }

  /*  2) E' ::= <epsilon> */
  if (ff_ok (2)) {
    return true;
  }

  syntax_error (1,2,-1,-1);
  return false; /* Syntax Error */
}

static bool f_T(void)
{
  /*  3) T  ::= F T' */
  if (ff_ok (3)) {
    return f_F() && f_T1();
  }

  syntax_error (3,-1,-1,-1);
  return false; /* Syntax Error */
}

static bool f_T1(void)
{
  /*   4) T' ::= * F T' */
  if (ff_ok (4)) {
    return match ('*') && f_F() && f_T1();
  }

  /*   5) T' ::= <epsilon> */
  if (ff_ok (5)) {
    return true;
  }

  syntax_error (4,5,-1,-1);
  return false; /* Syntax Error */
}

bool f_F(void)
{
  /*  6) F  ::= ( E )  */
  if (ff_ok (6)) {
    return match ('(') && f_E() && match (')');
  }

  /*  7) F  ::= D  */
  if (ff_ok (7)) {
    return f_D();
  }

  syntax_error (7,8,-1,-1);
  return false; /* Syntax Error */
}

static bool f_D(void)
{
  /*  8)  D  ::= identifier D'   */
  if (ff_ok (8)) {
    return match (tok_identifier) && f_D1();
  }

  syntax_error (8,-1,-1,-1);
  return false; /* Syntax Error */
}

static bool f_D1(void)
{
  /*  9) D' ::= . identifier D' */
  if (ff_ok (9)) {
    return match ('.') && match (tok_identifier) && f_D1();
  }

  /*  10) D' ::= [ E ] D'    */
  if (ff_ok (10)) {
    return match ('[') && f_E() && match (']') && f_D1();
  }

  /*  11) D' ::= -> identifier D'  */
  if (ff_ok (11)) {
    return match (tok_arrow) && match (tok_identifier) && f_D1();
  }

  /*  12) D' ::=  <epsilon */
  if (ff_ok (12)) {
    return true;
  }

  syntax_error (9,10,11,12);
  return false; /* Syntax Error */
}


/***********************  E  N  D  ***********************************************/
