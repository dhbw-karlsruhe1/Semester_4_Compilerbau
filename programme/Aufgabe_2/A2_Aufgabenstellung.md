# Gleitkommazahlen (reg. Grammatik) (1 Punkte)
Geben Sie eine rechtsreguläre Grammatik an, mit welchen Sie Gleitkommazahlen darstellen können. Dabei soll die Grammatik der "vereinfachten Gleitkommazahlen", welche in der Vorlesung vorgestellt wurde, als Ausgangspunkt genommen werden. Damit das Ganze "handhabbar" bleibt, wird folgendes Alphabet angenommen:   V= { 0 1 + - e . }. (D.h. es kommen nur die Ziffern 0 und 1 vor) Beispiele von ableitbaren Wörtern:
       - 101
       - 101e111
       - 101.111
       - 1e+1

---

## Feedback:
> Die Ableitung der Beispiele fehlt. In der nächsten Aufgabe dann abgeben.

## Punkte:

1,00/1,00