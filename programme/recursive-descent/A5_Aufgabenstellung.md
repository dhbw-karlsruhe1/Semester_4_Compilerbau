# Rekursiver Abstiegsparser (4 Punkte)
Implementieren Sie einen Rekursiven Abstiegsparser für die Sprache, die mit folgender Grammatik G=(N,T,P,E) definiert ist:

```ebnf
N = {E T F D}

T = { + * ( )  [  ]  .  -> identifier}

P = {
    E ::= E "+" T   | T .
    T ::= T "*" F   | F .
    F ::= "(" E ")" | D .
    D ::= D "." identifier | D "[" E "]" | D "->" identifier | identifier .

}
```

Mit identifier sind C-Bezeichner gemeint.

Neben den vorhanden Test-Dateien schreiben Sie mindestens 2 "Testprogramme", die erfolgreich analysiert werden können und zwei weitere, die einen Syntaxfehler beschreiben.

Sie können die Dateien der Übungen "progamme/recursive-decent" benutzen.

<b><u>Achtung:</u> Sie müssen obige Grammatik benutzen und nicht die in der Datei "parser.c" erwähnte!</b>

Falls Sie die Grammatik transformieren, muss sie, ebenso wie die First-Follow-Mengen der Regeln, in der Datei "parser.c" als Kommentar angegeben werden  (vergleichbar wie die in der Vorlage vorhanden Kommentaren)

Erstellen Sie ein komprimiertes tar-Archiv:

Räumen Sie Ihre Verzeichnis zuerst auf: (In Ihrem Verzeichnis)

     make clean

dann gehen Sie in das Elternverzeichnis:

      tar -czvf  NAME-NAME-NAME.tgz  NAME-NAME-NAME

und laden Sie dieses Archiv hoch


Für die Abnahme tippe ich nach dem Auspacken in "Ihrem" Verzeichnis "make clean; make; make test", Alles muss fehlerfrei übersetzen; die Tests müssen entsprechend "durchgehen"; Damit mit "make" fortfährt, obwohl das Programm fehlschlug (main() also einen Wert != 0 zurückliefert), müssen Sie im Makefile das Programm so aufrufen

test:

       - parser test-xy

also mit einem BINDESRICH gefolgt von LEERZEICHEN vor dem Namen des aufgerufenen Programmes.

ich schaue mir die Quellen an.

Diese Programmieraufagbe darf in Gruppen 2 bis 3 Personen gelöst werden, wobei ich bei nach der Abnahme, jedem Teilnehmer der Gruppe fragen zum Quellcode stellen werde.

Die Aufgabe muss von allen abgegeben werden.

---

## Kommentar:

> A5_Dumke-Ludwig-Schnatterbeck-Simonis-Steigleder_1.tar.gz = Lösung bevor die Aufgabenstellung bekannt war\
> \
> A5_Dumke-Ludwig-Schnatterbeck-Simonis-Steigleder_2.tar.gz = Lösung gemäß der Aufgabenstellung.

---

## Feedback

> ok

## Punkte:

4,00/4,00