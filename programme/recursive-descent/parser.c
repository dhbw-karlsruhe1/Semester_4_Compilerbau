/* Project:  COCKTAIL training
 * Descr:    A recursive Decent Parser for expressions
 * Kind:     The parser (solution)
 * Author:   Dr. Juergen Vollmer <juergen.vollmer@informatik-vollmer.de>
 * $Id: parser.c.in,v 1.5 2017/11/22 10:15:01 vollmer Exp $
 */

# include <stdlib.h>
# include "expr_scan.h"
# include "parser.h"
# include "Errors.h"
      	/* contains: `Message', `MessageI', `xxFatal', `xxError' */

tToken CurToken;

/*****************************************************************************
 * helpers
 *****************************************************************************/

/* Debugging:  if the C-compiler is called with `-DDEBUG',
 *             some output will be generated
 */
# ifdef DEBUG
# define DEBUG_show(msg) printf ("%-5s cur = `%s'\n", msg, token2string (CurToken));
# else
# define DEBUG_show(msg)  /* no output */
# endif

static const char* token2string (tToken token)
/* maps token-code to token textual representation */
{
  switch (token) {
  case '+':
    return "+";
  case '*':
    return "*";
  case '(':
    return "(";
  case ')':
    return ")";
  case ']':
    return "]";
  case '[':
    return "[";
  case '.':
    return ".";
  case tok_arrow:
    return "->";
  case tok_identifier:
    return "identifier";
  case expr_scan_EofToken:
    return "EOF";
  default:
    /* this should never happen */
    MessageI ("FATAL ERROR: unknown token", xxFatal,
	      expr_scan_Attribute.Position, xxInteger, (char*) &token);
    abort(); /* aborts the program */
  }
}

/******************************************************************************/

bool match (tToken token)
/* Instead of writing for each token 't' a function 'f_t()', we use
 * 'match (t)'.
 */
{
  DEBUG_show ("MATCH");
# ifdef DEBUG
  printf ("      cur = `%s' expected = `%s'\n", token2string(CurToken), token2string (token));
# endif
  if (CurToken == token) {
    if (CurToken != expr_scan_EofToken) CurToken = expr_scan_GetToken();
    DEBUG_show ("  new");
    return true;
  } else {
    char msg[255];
    sprintf (msg, "Syntax Error: found: `%s' expected: `%s'",
	     token2string (CurToken), token2string (token));
    Message (msg, xxError, expr_scan_Attribute.Position);
    return false;  /* Syntax Error */
  }
}

/*****************************************************************************
 * First-Follow-set Tests
 *   1. FIRST (T E' FOLLOW(E)) = { ( identifier }
 *   2. FIRST ("+" T E' FOLLOW(E')) = { + }
 *   3. FIRST (ε FOLLOW(E')) = { # ) ] }
 *   4. FIRST (F T' FOLLOW(T)) = { ( identifier }
 *   5. FIRST ("*" F T' FOLLOW(T')) = { * }
 *   6. FIRST (ε FOLLOW (T')) = { # ) ] + }
 *   7. FIRST ("(" E ")" FOLLOW(F)) = { ( }
 *   8. FIRST (D FOLLOW(F)) = { identifier }
 *   9. FIRST (identifier D' FOLLOW(D)) = { identifier }
 *   10. FIRST ( "." identifier D' FOLLOW(D')) = { . }
 *   11. FIRST ("[" E "]" D' FOLLOW(D')) = { [ }
 *   12. FIRST ("->" identifier D' FOLLOW(D')) = { -> }
 *   13. FIRST (ε FOLLOW (D')) = { # ) ] + * }
 *
 * The function 'FirstFollow_i ()' returns true, if and only if 'CurToken'
 * is element of the appropriate set.
 *
 *   #   <==> expr_scan_EofToken
 *   identifier <==> tok_identifier
 ******************************************************************************/

static bool FirstFollow_1 (void)
{
  switch (CurToken) {
  case '(':
  case tok_identifier:
    return true;
  default:
    return false;
  }
}

/* fill in the other FirstFollow-functions: FirstFollow_2() .. FirstFollow_8() */
static bool FirstFollow_2 (void)
{
  switch (CurToken) {
  case '+': 
    return true;
  default:
    return false;
  }
}

static bool FirstFollow_3 (void)
{
  switch (CurToken) {
  case expr_scan_EofToken: 
  case ')': 
  case ']':
    return true;
  default:
    return false;
  }
}

static bool FirstFollow_4 (void)
{
  switch (CurToken) {
  case '(': 
  case tok_identifier:
    return true;
  default:
    return false;
  }
}

static bool FirstFollow_5 (void)
{
  switch (CurToken) {
  case '*': 
    return true;
  default:
    return false;
  }
}

static bool FirstFollow_6 (void)
{
  switch (CurToken) {
  case expr_scan_EofToken: 
  case ')': 
  case ']':
  case '+':
    return true;
  default:
    return false;
  }
}

static bool FirstFollow_7 (void)
{
  switch (CurToken) {
  case '(': 
    return true;
  default:
    return false;
  }
}

static bool FirstFollow_8 (void)
{
  switch (CurToken) {
  case tok_identifier:
    return true;
  default:
    return false;
  }
}

static bool FirstFollow_9 (void)
{
  switch (CurToken) {
  case tok_identifier:
    return true;
  default:
    return false;
  }
}

static bool FirstFollow_10 (void)
{
  switch (CurToken) {
  case '.':
    return true;
  default:
    return false;
  }
}

static bool FirstFollow_11 (void)
{
  switch (CurToken) {
  case '[':
    return true;
  default:
    return false;
  }
}

static bool FirstFollow_12 (void)
{
  switch (CurToken) {
  case tok_arrow:
    return true;
  default:
    return false;
  }
}

static bool FirstFollow_13 (void)
{
  switch (CurToken) {
  case ')':
  case ']':
  case '+':
  case '*':
  case expr_scan_EofToken:
    return true;
  default:
    return false;
  }
}

/******************************************************************************
 * We use the following grammar for arithmetic expressions:
 *   1. E ::= T E'
 *   2. E' ::= "+" T E'
 *   3. E' ::= ε
 *   4. T ::= F T'
 *   5. T' ::= "*" F T'
 *   6. T' ::= ε
 *   7. F ::= "(" E ")"
 *   8. F ::= D
 *   9. D ::= identifier D'
 *   10. D' ::= "." identifier D'
 *   11. D' ::= "[" E "]" D'
 *   12. D' ::= "->" identifier D'
 *   13. D' ::= ε
 ******************************************************************************/

/* The function 'f_nt()' for the non-terminal 'nt' returns true, iff
 * 'CurToken' starts a sequence of input-tokens which may be generated by 'nt'.
 * 'CurToken' refers after that to the next token to be processed.
 * Note: eof = end-of-file  is a token too.
 */

bool f_E (void)
{
  DEBUG_show("E");

  if (FirstFollow_1()) return f_T() && f_E1();
  
  Message ("Syntax Error in <E>", xxError, expr_scan_Attribute.Position);
  return false; /* Syntax Error */
}

/* fill in the functions for the other non-terminals */

bool f_E1 (void)
{
  DEBUG_show("E1");

  if (FirstFollow_2()) return f_Plus() && f_T() && f_E1();
  if (FirstFollow_3()) return true;
  Message ("Syntax Error in <E1>", xxError, expr_scan_Attribute.Position);
  return false; /* Syntax Error */
}

bool f_T (void)
{
  DEBUG_show("T");

  if (FirstFollow_4()) return f_F() && f_T1();
  Message ("Syntax Error in <T>", xxError, expr_scan_Attribute.Position);
  return false; /* Syntax Error */
}

bool f_T1 (void)
{
  DEBUG_show("T1");

  if (FirstFollow_5()) return f_Mal() && f_F() && f_T1();
  if (FirstFollow_6()) return true;
  
  Message ("Syntax Error in <T1>", xxError, expr_scan_Attribute.Position);
  return false; /* Syntax Error */
}

bool f_F (void)
{
  DEBUG_show("F");

  if (FirstFollow_7()) return f_KlammerAuf() && f_E() && f_KlammerZu();
  if (FirstFollow_8()) return f_D();
  
  Message ("Syntax Error in <F>", xxError, expr_scan_Attribute.Position);
  return false; /* Syntax Error */
}

bool f_D (void)
{
  DEBUG_show("D");

  if (FirstFollow_9()) return f_ID() && f_D1();
  
  Message ("Syntax Error in <D>", xxError, expr_scan_Attribute.Position);
  return false; /* Syntax Error */
}

bool f_D1 (void)
{
  DEBUG_show("D1");

  if (FirstFollow_10()) return f_Punkt() && f_ID() && f_D1();
  if (FirstFollow_11()) return f_EckigeKlammerAuf() && f_E() && f_EckigeKlammerZu() && f_D1();
  if (FirstFollow_12()) return f_Arrow() && f_ID() && f_D1();
  if (FirstFollow_13()) return true;
  
  Message ("Syntax Error in <D1>", xxError, expr_scan_Attribute.Position);
  return false; /* Syntax Error */
}

bool f_Punkt (void) 
{
  return match('.');
}

bool f_Plus (void) 
{
  return match('+');
}

bool f_Mal (void) 
{
  return match('*');
}

bool f_KlammerAuf (void) 
{
  return match('(');
}

bool f_KlammerZu (void) 
{
  return match(')');
}

bool f_EckigeKlammerAuf (void) 
{
  return match('[');
}

bool f_EckigeKlammerZu (void) 
{
  return match(']');
}

bool f_ID (void) 
{
  return match(tok_identifier);
}

bool f_Arrow (void) 
{
  return match(tok_arrow);
}

/***********************  E  N  D  ***********************************************/
